/*
 * Virtual screen sessions.
 *
 * A session has a pseudoterminal (pty), a virtual screen,
 * and a process that is the session leader.  Output from the
 * pty goes to the virtual screen, which can be one of several
 * virtual screens multiplexed onto the physical screen.
 * At any given time there is a particular session that is
 * designated the "foreground" session.  The contents of the
 * virtual screen associated with the foreground session is what
 * is shown on the physical screen.  Input from the physical
 * keyboard is directed to the pty for the foreground session.
 */

#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/signal.h>
#include <sys/wait.h>

#include "session.h"

SESSION *sessions[MAX_SESSIONS];  // Table of existing sessions
SESSION *fg_session;              // Current foreground session

/*
 * Initialize a new session whose session leader runs a specified command.
 * If the command is NULL, then the session leader runs a shell.
 * The new session becomes the foreground session.
 */
SESSION *session_init(char *path, char *argv[]) {
    for (int i = 0; i < MAX_SESSIONS; i++) {
        if (sessions[i] == NULL) {
            int mfd = posix_openpt(O_RDWR | O_NOCTTY);
            if (mfd == -1)
                return NULL; // No more ptys
            if ( unlockpt(mfd) < 0 ) {
                return NULL;
            }
            char *sname = ptsname(mfd);
            if (sname == NULL) return NULL; //error in ptsname

            // Set nonblocking I/O on master side of pty
            if (fcntl(mfd, F_SETFL, O_NONBLOCK) < 0) return NULL;

            SESSION *session = calloc(sizeof(SESSION), 1);
            if (session == NULL) return NULL;
            sessions[i] = session;
            session->sid = i;
            session->vscreen = vscreen_init();
            session->ptyfd = mfd;
            session->error = 0;

            // Fork process to be leader of new session.
            if ((session->pid = fork()) == 0) {
                // Open slave side of pty, create new session,
                // and set pty as controlling terminal.
                int sfd = open(sname, O_RDWR);
                if (sfd < 0) {
                    fprintf(stderr, "error in opening file. (sfd<0) @ session_init().\n");
                    exit(1);
                }

                if (setsid() < 0) {
                    fprintf(stderr, "setsid failed @ session_init().\n");
                    exit(1);
                }

                if (ioctl(sfd, TIOCSCTTY, 0) < 0) {
                    fprintf(stderr, "ioctl failed @ session_init().\n");
                    exit(1);
                }

                if ( dup2(sfd, 2) < 0 ) {
                    fprintf(stderr, "dup2 call to open stderr got and error @ session_init().\n");
                    exit(1);
                }

                // Set up stdin/stdout and do exec.
                // stdoout
                if ( dup2(sfd, 1) < 0 ) {
                    fprintf(stderr, "dup2 call to open stdout got and error @ session_init().\n");
                    exit(1);
                }
                //stdin
                if ( dup2(sfd, 0) < 0 ) {
                    fprintf(stderr, "dup2 call to open stdin got and error @ session_init().\n");
                    exit(1);
                }

                if (close(sfd) < 0) {
                    fprintf(stderr, "closing sfd failed @ session_init().\n");
                    exit(1);
                }

                if (close(mfd) < 0) {
                    fprintf(stderr, "closing mfd failed @ session_init().\n");
                    exit(1);
                }

                //close(mfd);

                // Set TERM environment variable to match vscreen terminal
                // emulation capabilities (which currently aren't that great).

                // if (putenv("TERM=dumb") != 0) {
                if (putenv("TERM=ansi") != 0) {
                    fprintf(stderr, "putenv failed @ session_init().\n");
                    exit(1);
                }



                // char *myargv[] = {"/bin/ps", "a", NULL};
                // if((execv(myargv[0], myargv)) < 0){
                if ( execv(path, argv) < 0) {
                    fprintf(stderr, "execv called return an error @ session_init(). \n");
                    set_status("execv called return an error @ session_init().");
                    exit(1);
                }


                // TO BE FILLED IN
                fprintf(stderr, "EXEC FAILED (did you fill in this part?)\n");
                exit(1);
            }
            // Parent drops through
            session_setfg(session);
            return session;
        }
    }
    return NULL;  // Session table full.
}

/*
 * Set a specified session as the foreground session.
 * The current contents of the virtual screen of the new foreground session
 * are displayed in the physical screen, and subsequent input from the
 * physical terminal is directed at the new foreground session.
 */
void session_setfg(SESSION *session) {
    fg_session = session;
    vscreen_show(fg_session->vscreen);
    // REST TO BE FILLED IN
}

/*
 * Read up to bufsize bytes of available output from the session pty.
 * Returns the number of bytes read, or EOF on error.
 */
int session_read(SESSION *session, char *buf, int bufsize) {
    return read(session->ptyfd, buf, bufsize);
}

/*
 * Write a single byte to the session pty, which will treat it as if
 * typed on the terminal.  The number of bytes written is returned,
 * or EOF in case of an error.
 */
int session_putc(SESSION *session, char c) {
    // TODO: Probably should use non-blocking I/O to avoid the potential
    // for hanging here, but this is ignored for now.
    nodelay(main_screen, FALSE);
    int n = write(session->ptyfd, &c, 1);
    nodelay(main_screen, TRUE);
    return n;
}

/*
 * Forcibly terminate a session by sending SIGKILL to its process group.
 */
void session_kill(SESSION *session) {
    // TO BE FILLED IN
    kill(-session->pid, SIGKILL);
    //sleep(1); //wait till signal is sent
    //session_fini(session);
}

/*
 * Deallocate a session that has terminated.  This function does not
 * actually deal with marking a session as terminated; that would be done
 * by a signal handler.  Note that when a session does terminate,
 * the session leader must be reaped.  In addition, if the terminating
 * session was the foreground session, then the foreground session must
 * be set to some other session, or to NULL if there is none.
 */
void session_fini(SESSION *session) {
    // TO BE FILLED IN

    //bloack the signal
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    int index = session->sid;
    vscreen_fini(session->vscreen);
    if ( close(session->ptyfd) < 0 ) {
        fprintf(stderr, "Error in closing ptyfd @ sessions_fini\n");
    }
    free(session);
    sessions[index] = NULL;

    //unbloack the signals
    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}

//helper function to reset the cursor
void reset_cursor() {
    //This whill reset cursor position and will make changes to screen if nessesary
    vscreen_sync(fg_session->vscreen);
}

//Finds the non foreground session in sessions.
SESSION *find_nonfg_session() {
    SESSION *session = NULL;
    for ( int i = 0; i < MAX_SESSIONS; i++) {
        session = sessions[i];
        if ( (session != NULL) && (session != fg_session)) {
            return session;
        }
    }
    return NULL;
}



