/*
 * Ecran: A program that (if properly completed) supports the multiplexing
 * of multiple virtual terminal sessions onto a single physical terminal.
 */

#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <sys/signal.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <limits.h>
#include <time.h>
#include <sys/wait.h>

#include "ecran.h"
#include "debug.h"

static void initialize();
static void curses_init(void);
static void curses_fini(void);
static void finalize(void);

char *myArgv[MAX_ARGV];
int print_error = 0;

int main(int argc, char *argv[]) {
    initialize();
    parse_args(argc, argv);
    setup_signals();
    update_time(); // update the status with time
    alarm(1);
    usleep(150000);  // having this removes args in first line, minimum 100500
    if ( myArgv[0] != NULL) input_args();
    mainloop();
    // NOT REACHED
}

/*
 * Initialize the program and launch a single session to run the
 * default shell.
 */
static void initialize() {
    curses_init();
    char *path = getenv("SHELL");
    if (path == NULL)
        path = "/bin/bash";
    char *argv[2] = { " (ecran session)", NULL };
    session_init(path, argv);
}

/*
 * Cleanly terminate the program.  All existing sessions should be killed,
 * all pty file descriptors should be closed, all memory should be deallocated,
 * and the original screen contents should be restored before terminating
 * normally.  Note that the current implementation only handles restoring
 * the original screen contents and terminating normally; the rest is left
 * to be done.
 */
static void finalize(void) {
    // REST TO BE FILLED IN
    SESSION *session = NULL;
    //Send sigkill signal to all running child - as per docs
    for (int i = 0; i < MAX_SESSIONS; i++) {
        session = sessions[i];
        if ( session != NULL) {
            session_kill(session);
            usleep(1000); //wait till all child receive sigkill signal and SIGCHLD handle mark them as an error
            //Race condition here. Need this so SIGKILL signal can be handle properly.
            //kill(session->pid, SIGKILL);
            //session_fini(session);
        }
    }
    //clear all the child data
    for (int i = 0; i < MAX_SESSIONS; i++) {
        session = sessions[i];
        if ( session != NULL) {
            //session_kill(session);
            //kill(session->pid, SIGKILL);
            session_fini(session);
            if (print_error) {fprintf(stderr, "session %d is finalized\n", i);}
        }
    }

    curses_fini();
    exit(EXIT_SUCCESS);
}

/*
 * Helper method to initialize the screen for use with curses processing.
 * You can find documentation of the "ncurses" package at:
 * https://invisible-island.net/ncurses/man/ncurses.3x.html
 */
static void curses_init(void) {
    initscr();
    raw();                       // Don't generate signals, and make typein
    // immediately available.
    noecho();                    // Don't echo -- let the pty handle it.
    main_screen = stdscr;
    nodelay(main_screen, TRUE);  // Set non-blocking I/O on input.
    wclear(main_screen);         // Clear the screen.
    refresh();                   // Make changes visible.

    //main window
    main_screen = newwin(LINES - 1, COLS, 0, 0);
    if (main_screen == NULL) {
        if (print_error) {fprintf(stderr, "newin of main_screen retuened an error\n");}
    }
    nodelay(main_screen, TRUE);
    wclear(main_screen);
    refresh();

    //setting up status window
    status_screen = newwin(1, COLS, LINES - 1, 0);
    nodelay(status_screen, TRUE);
    wclear(status_screen);
    refresh();
    set_status("Welcome to awesome screen by Tirth Darji");
}

/*
 * Helper method to finalize curses processing and restore the original
 * screen contents.
 */
void curses_fini(void) {
    delwin(status_screen);
    delwin(main_screen);
    delwin(stdscr);
    endwin();
}

/*
 * Function to read and process a command from the terminal.
 * This function is called from mainloop(), which arranges for non-blocking
 * I/O to be disabled before calling and restored upon return.
 * So within this function terminal input can be collected one character
 * at a time by calling getch(), which will block until input is available.
 * Note that while blocked in getch(), no updates to virtual screens can
 * occur.
 */
void do_command() {
    // Quit command: terminates the program cleanly
    char c = wgetch(main_screen);
    if (c == 'q') finalize();
    else if (c == 'n') create_terminal();
    else if ( (c - 18) >= 0 && (c - 48) < MAX_SESSIONS) switch_screen(c - 48);
    else if ( c == 'k' ) handle_kill();
    else if ( c == 'h') display_help();
    else flash();
}

/*
 * Function called from mainloop(), whose purpose is do any other processing
 * that has to be taken care of.  An example of such processing would be
 * to deal with sessions that have terminated.
 */
void do_other_processing() {
    // Main process is to rip closed sessions
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    //check if any session has error or terminated
    //if so then clean those sessions

    SESSION *session = NULL;
    for (int i = 0; i < MAX_SESSIONS; i++) {
        session = sessions[i];
        if ( session != NULL && session->error) {
            //If forground session has error then change foreground
            if (print_error) {fprintf(stderr, "session %d has and error and being cleaned\n", i);}
            if (session == fg_session) {
                SESSION *newfg = find_nonfg_session();
                if (newfg == NULL) finalize(); //the only remain terminal is being closed. Exit the program cleanly.
                session_setfg(newfg);
            }
            int index = session->sid;
            session_fini(session);
            vscreen_show(fg_session->vscreen);
            //update the status
            char *change1 = "sesion no: ";
            char *change2 = " closed. Current session is ";
            char stat[COLS];
            sprintf(stat, "%s%d%s%d", change1, index, change2, fg_session->sid );
            set_status(stat);
            update_time();
            reset_cursor();
        }
    }
    //unmask the blocked signal
    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}


//Parsing the arguments
void parse_args(int argc, char *argv[])
{
    char option;
    char *output_file;
    while ((option = getopt(argc, argv, "+o:")) != -1) {
        switch (option) {
        case 'o': {
            output_file = optarg;
            //debug("output_file %s\n", output_file);
            int fd = open(output_file, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
            if ( fd < 0) {
                //error happened
                fprintf(stderr, "Erron in opening filname @ ecran.c: Parse_args\n");
                //set_status("error in opening output file");
            } else {
                //file opened, now dup the stderr
                if ( dup2(fd, 2) < 0 ) {
                    set_status("error in opening output file");
                }
                //set_status("stderr is being redirected to given file");
                print_error = 1;
                if (print_error) {fprintf(stderr, "Error redirection to this file started\n");}
            }
            break;
        }
        default: {
            break;
        }
        }
    }
    int i = 0;
    for (int index = optind; (index < argc) && (index < MAX_ARGV - 1); index++) {
        if (print_error)  {fprintf (stderr, "Non-option argument %s\n", argv[index]);}
        myArgv[i++] = argv[index];
    }
    myArgv[i] = NULL;
    //fprintf (stderr,"Non-option argument %s\n", argv[index]);
}

//Creating new terminal for CTRL-a n
void create_terminal() {

    //bloack all signal so while creating new session, termination of session doesnt arrive
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    char *path = getenv("SHELL");
    if (path == NULL) path = "/bin/bash";
    char *argv[2] = { " (ecran session)", NULL };
    //char *argv[2] = { NULL, NULL };
    SESSION *session = session_init(path, argv);
    if (session == NULL) {
        //error creating new terminal
        set_status("Error creating new terminal");
        flash();
    } else {
        char *change = "Welcome to new session ";
        char stat[COLS];
        sprintf(stat, "%s%d", change, session->sid);
        set_status(stat);
        refresh();
        if (print_error)  {fprintf(stderr, "New session %d, pid %d created successfully\n", session->sid, session->pid);}
    }
    update_time(); // time update
    reset_cursor();

    //unmask the blocked signal
    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}

//swtching to new terminals
void switch_screen(int sid) {

    //bloack all signal so while switching sessions
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    SESSION *session = sessions[sid];
    if (session == NULL) {
        char *ch = "Requested session is not in use. Current session is ";
        char stat[COLS];
        sprintf(stat, "%s%d", ch, fg_session->sid);
        set_status(stat);
        flash();
    } else {
        session_setfg(session);
        char *change = "Session has changed to ";
        char stat[COLS];
        sprintf(stat, "%s%d", change, sid);
        set_status(stat);
    }
    update_time();
    reset_cursor();

    //unmask the blocked signal
    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}

//setup the needed signals
void setup_signals() {
    signal(SIGCHLD, handle_sigchld);
    signal(SIGTERM, handle_sigterm);
    signal(SIGINT, handle_sigterm);
    signal(SIGALRM, handle_sigalarm);
}

//helper to get and send kill signal CTRL-a k n
void handle_kill() {
    char c = wgetch(main_screen);
    if ( (c - 18) >= 0 && (c - 48) < MAX_SESSIONS) {
        if (sessions[c - 48] != NULL) session_kill(sessions[c - 48]); // have to send SIGKILL as per doc.
        else {
            flash();
            char *change = "given session id doesnt exist. Current session is ";
            char stat[COLS];
            sprintf(stat, "%s%d", change, fg_session->sid);
            set_status(stat);
        }
    } else {
        set_status("session id should be number from 0 to 9");
        flash();
    }
    reset_cursor();
}


void handle_sigterm() {

    //bloack all signal so no signal can deliver while closing terminal
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    finalize();

    //never reached here
    if (print_error)  {fprintf(stderr, "SIGTERM did not closed ecran successfully\n" );}
    //unmask the blocked signal
    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}

void handle_sigalarm() {
    //call the update time method in vscreen
    update_time();
    reset_cursor(); //reset cursor to original point
    alarm(1);
}

//hangle for the SIGCHLD
void handle_sigchld(int sig) {
    int olderrno = errno;
    sigset_t mask_all, prev_all;
    sigfillset(&mask_all);
    sigprocmask(SIG_BLOCK, &mask_all, &prev_all);

    pid_t pid;
    SESSION *session;
    int status = 0;

    while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
        if (WIFSIGNALED(status)) {
            for (int i = 0; i < MAX_SESSIONS; i++) {
                session = sessions[i];
                if (  (session != NULL) && session->pid == pid) { //checking null value before pid is important
                    session->error = 1;
                    if (print_error) {fprintf(stderr, "Session %d, pid %d exited with status %d and WIFSIGNALED %d.\n", i, pid, WEXITSTATUS(status), WIFSIGNALED(status));}
                }
            }
        }
    }

    sigprocmask(SIG_SETMASK, &prev_all, NULL);
    errno = olderrno;
}

//temporary fix for getting argument from user
void input_args() {
    char* pointer = myArgv[0];
    for (int i = 0; (pointer != NULL) && (i < MAX_ARGV) ; i++) {
        char *c = pointer;
        while ( *c != '\0') {
            session_putc(fg_session, *c);
            c++;
        }
        session_putc(fg_session, ' ');
        pointer = myArgv[i + 1];
    }
    session_putc(fg_session, '\n');
}

//This function displays the help
//It only closes the help when ESC key is pressed.
void display_help() {

    //open the help display
    set_status("Help Display. Press Esc to return");
    update_time();
    wclear(main_screen);
    mvwprintw(main_screen, 0, 0, "Summary of the commands understand by ecran");
    mvwprintw(main_screen, 2, 0, "Flags:");
    mvwprintw(main_screen, 3, 0, "-o filename : This flag transfers theh stderr to given filename." );
    mvwprintw(main_screen, 4, 14, "If filename doesn't exist then it will be created.");
    mvwprintw(main_screen, 6, 0, "Extra arguments:");
    mvwprintw(main_screen, 7, 0, "If program starts with commandline arguments arg[0] arg[1].. then");
    mvwprintw(main_screen, 8, 0, "then it will be executed in the initial virtual session");
    mvwprintw(main_screen, 9, 0, "Command-line arguments can be followed by flag -o filename.");
    mvwprintw(main_screen, 11, 0, "Terminal control commands:");
    mvwprintw(main_screen, 12, 0, "CTRL-a n   -> creates new virtual terminal.");
    mvwprintw(main_screen, 13, 0, "CTRL-a x   -> switches to session x. MAX_SESSIONS = 10, Numbered 0 to 9.");
    mvwprintw(main_screen, 14, 0, "CTRL-a k x -> kills x numbered virtual session if it is active.");
    mvwprintw(main_screen, 15, 0, "CTRL-a q   -> to close the program.");
    mvwprintw(main_screen, 17, 0, "Active session ids:");
    int index = 0;
    for (int i = 0; i < MAX_SESSIONS; i++) {
        if ((sessions[i] != NULL) && (sessions[i]->error == 0)) {
            mvwaddch(main_screen, 18, index, i + 48 );
            index += 3;
        }
    }
    wmove(main_screen, 0, 0);
    wrefresh(main_screen);

    char c = wgetch(main_screen);
    while ( (c - 27) != 0) {        // 27 is ASCII code for escap key
        flash();
        c = wgetch(main_screen);
    }

    //close the display
    vscreen_show(fg_session->vscreen);
    char *change = "Current session is ";
    char stat[COLS];
    sprintf(stat, "%s%d", change, fg_session->sid);
    set_status(stat);
    update_time();
    reset_cursor();

}