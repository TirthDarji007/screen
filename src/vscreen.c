#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

#include "vscreen.h"

/*
 * Functions to implement a virtual screen that can be multiplexed
 * onto a physical screen.  The current contents of a virtual screen
 * are maintained in an in-memory buffer, from which they can be
 * used to initialize the physical screen to correspond.
 */

WINDOW *main_screen;
WINDOW *status_screen;
char *status_line = NULL;
int print_error;

//Global variables to store escap sequence.
int esc_entered = 0;
int esc_seq = 0;
char escval[10];
int escval_index = 0;
int val1 = 0;
int val2 = 0;
int stored_cur_col = 0;
int stored_cur_row = 0;

struct vscreen {
    int num_lines;
    int num_cols;
    int cur_line;
    int cur_col;
    char **lines;
    char *line_changed;
};

static void update_line(VSCREEN *vscreen, int l);

/*
 * Create a new virtual screen of the same size as the physical screen.
 */
VSCREEN *vscreen_init() {
    VSCREEN *vscreen = calloc(sizeof(VSCREEN), 1);
    vscreen->num_lines = LINES - 1; //;// - 1;
    vscreen->num_cols = COLS;
    vscreen->cur_line = 0;
    vscreen->cur_col = 0;
    vscreen->lines = calloc(sizeof(char *), vscreen->num_lines);
    vscreen->line_changed = calloc(sizeof(char), vscreen->num_lines);
    for (int i = 0; i < vscreen->num_lines; i++)
        vscreen->lines[i] = calloc(sizeof(char), vscreen->num_cols);
    return vscreen;
}

/*
 * Erase the physical screen and show the current contents of a
 * specified virtual screen.
 */
void vscreen_show(VSCREEN *vscreen) {
    wclear(main_screen);
    for (int l = 0; l < vscreen->num_lines; l++) {
        //if (vscreen->line_changed[l]) {
        if (1) {
            update_line(vscreen, l);
            vscreen->line_changed[l] = 0;
        }
    }
    wmove(main_screen, vscreen->cur_line, vscreen->cur_col);
    refresh();
    wrefresh(main_screen);
}

/*
 * Function to be called after a series of state changes,
 * to cause changed lines on the physical screen to be refreshed
 * and the cursor position to be updated.
 * Although the same effect could be achieved by calling vscreen_show(),
 * the present function tries to be more economical about what is displayed,
 * by only rewriting the contents of lines that have changed.
 */
void vscreen_sync(VSCREEN *vscreen) {

    for (int l = 0; l < vscreen->num_lines; l++) {
        if (vscreen->line_changed[l]) {
            update_line(vscreen, l);
            vscreen->line_changed[l] = 0;
        }
    }
    wmove(main_screen, vscreen->cur_line, vscreen->cur_col);
    refresh();
    wrefresh(main_screen);
}

/*
 * Helper function to clear and rewrite a specified line of the screen.
 */
static void update_line(VSCREEN *vscreen, int l) {
    char *line = vscreen->lines[l];
    wmove(main_screen, l, 0);
    wclrtoeol(main_screen);
    for (int c = 0; c < vscreen->num_cols; c++) {
        char ch = line[c];
        if (isprint(ch))
            waddch(main_screen, line[c]);
    }
    wmove(main_screen, vscreen->cur_line, vscreen->cur_col);
    refresh();
    wrefresh(main_screen);
}

/*
 * Output a character to a virtual screen, updating the cursor position
 * accordingly.  Changes are not reflected to the physical screen until
 * vscreen_show() or vscreen_sync() is called.  The current version of
 * this function emulates a "very dumb" terminal.  Each printing character
 * output is placed at the cursor position and the cursor position is
 * advanced by one column.  If the cursor advances beyond the last column,
 * it is reset to the first column.  The only non-printing characters
 * handled are carriage return, which causes the cursor to return to the
 * beginning of the current line, and line feed, which causes the cursor
 * to advance to the next line and clear from the current column position
 * to the end of the line.  There is currently no scrolling: if the cursor
 * advances beyond the last line, it wraps to the first line.
 */
void vscreen_putc(VSCREEN *vscreen, char ch) {
    int l = vscreen->cur_line;
    int c = vscreen->cur_col;

    if (esc_seq) {
        // for (int i = 0; i < 10; i++) escval[i] = ' ';
        handle_escseq(vscreen, ch);
        return;
    }
    if (esc_entered) {
        // if ESC enter then check for [
        if ( ch == '[') { esc_seq = 1; esc_entered = 0;}
        else {esc_entered = 0; esc_seq = 0; vscreen_putc(vscreen, ch);}
        return;
    }

    if (isprint(ch)) {
        vscreen->lines[l][c] = ch;
        if (vscreen->cur_col + 1 < vscreen->num_cols)
            vscreen->cur_col++;
    } else if (ch == '\n') {
        if ( vscreen->num_lines == (vscreen->cur_line + 1)) {//1)) {
            vscreen_scroll(vscreen);
        } else {
            l = vscreen->cur_line = (vscreen->cur_line + 1) % vscreen->num_lines;
            memset(vscreen->lines[l], 0, vscreen->num_cols);
        }
    } else if (ch == '\r') {
        vscreen->cur_col = 0;
    } else if (ch == '\a') {
        flash();
    } else if (ch == '\b') {
        vscreen->lines[l][c] = '\0';
        vscreen->cur_col--;
    } else if (ch == '\t') {
        for (int i = 0; i < 8; i++) vscreen_putc(vscreen, ' ');
        // vscreen_sync(vscreen);
        return;
        // vscreen->cur_col = ((vscreen->cur_col + 8) < (COLS - 1)) ?  (vscreen->cur_col + 8) : COLS - 1;
    } else if (ch == '\f') {
        vscreen->cur_line++;
        if (vscreen->cur_line == vscreen->num_lines) {
            vscreen_scroll(vscreen);
            vscreen->cur_line = vscreen->num_lines - 1;
        }
        int col = vscreen->cur_col;
        vscreen->cur_col = 0;
        for (int i = 0; i < col; i++) vscreen_putc(vscreen, ' ');
        /*
        for (int i = 0; i < vscreen->num_lines; i++) {
            vscreen_clear_line(vscreen, i);
        }
        */
    } else if (ch == 27) { //((ch - 27) == 0) {
        esc_entered = 1;
    }
    vscreen->line_changed[l] = 1;
}

/*
 * Deallocate a virtual screen that is no longer in use.
 */
void vscreen_fini(VSCREEN *vscreen) {
    // TO BE FILLED IN

    //Free the pointers
    for (int i = 0; i < vscreen->num_lines; i++) free(vscreen->lines[i]);
    free(vscreen->line_changed);
    free(vscreen->lines);
    free(vscreen);
}


//Helpter fucntion to scroll screen up
//this function moves lines up and firs line will removed.
void vscreen_scroll(VSCREEN *vscreen) {
    for (int l = 0; l < vscreen->num_lines; l++) {
        if ( l == (vscreen->num_lines - 1)) {
            vscreen_clear_line(vscreen, l);
        } else {
            vscreen_clear_line(vscreen, l);
            strncpy(vscreen->lines[l], vscreen->lines[l + 1], vscreen->num_cols);
        }
        vscreen->line_changed[l] = 1;
    }
}

//Helper function to clear the line
void vscreen_clear_line(VSCREEN *vscreen, int l) {
    char *line = vscreen->lines[l];
    for (int c = 0; c < vscreen->num_cols; c++) {
        line[c] = '\0';
    }
    vscreen->line_changed[l] = 1;
}

//setting the status
void set_status(char *status) {
    char *stat_text = "Status: ";
    char stat[COLS];
    //sprintf(stat, "%s%s", stat_text, status);
    snprintf(stat, COLS - 1, "%s%s", stat_text, status);
    stat[COLS - 1] = '\0'; //expeciatly end the line.
    wclear(status_screen);
    mvwprintw(status_screen, 0, 0, stat);
    wrefresh(status_screen);
    refresh();
    status_line = status; //for letter use
}

//update the time
void update_time() {
    time_t current_time;
    struct tm * time_info;
    char timeString[9];  // space for "HH:MM:SS\0"
    time(&current_time);
    time_info = localtime(&current_time);
    strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);
    for (int i = 0; i < 9; i++) {
        mvwaddch(status_screen, 0, COLS - i, timeString[8 - i]);
    }
    wrefresh(status_screen);
    refresh();
}


//This function hadnle the escape sequence
//Caller will make sure ESC is entered before this function
void handle_escseq(VSCREEN *vscreen, char ch) {
    escval[escval_index++] = ch;
    // if (print_error) {fprintf(stderr, "escap sequence is %s\n", escval);}
    if ( !strcmp(escval, "s")) {
        // ESC[s = store the cursor position
        if (print_error) {fprintf(stderr, "matched to ESC[s\n");}
        stored_cur_col = vscreen->cur_col;
        stored_cur_row = vscreen->cur_line;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "u")) {
        //Esc[u = restore the stored cursor postion
        if (print_error) {fprintf(stderr, "matched to ESC[u\n");}
        vscreen->cur_line = stored_cur_row;
        vscreen->cur_col = stored_cur_col;
        // wmove(main_screen, vscreen->cur_line, vscreen->cur_col); //move it to correct position
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "2J") ) {
        //Esc[2J    Erase Display: Clears the screen and moves the cursor to the home position (line 0, column 0).
        if (print_error) {fprintf(stderr, "matched to ESC[2J\n");}
        for (int i = 0; i < vscreen->num_lines; i++) vscreen_clear_line(vscreen, i);
        vscreen->cur_line = vscreen->cur_col = 0;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "J") || !strcmp(escval, "0J") ) {
        //Esc[0J    Erase Display: fromt the cursor to end of the screen
        if (print_error) {fprintf(stderr, "matched to ESC[J or Esc[0J\n");}
        char *line = line = vscreen->lines[vscreen->cur_line];
        for (int i = vscreen->cur_col; i < vscreen->num_cols; i++) line[i] = '\0';
        vscreen->line_changed[vscreen->cur_line] = 1;
        for (int i = vscreen->cur_line + 1; i < vscreen->num_lines; i++) vscreen_clear_line(vscreen, i);
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "1J") ) {
        //Esc[1J    Erase Display: begining of screen to end cursor
        if (print_error) {fprintf(stderr, "matched to ESC[1J\n");}
        for (int i = 0; i < vscreen->cur_line; i++) vscreen_clear_line(vscreen, i);
        char *line = line = vscreen->lines[vscreen->cur_line];
        for (int i = 0; i <= vscreen->cur_col; i++) line[i] = '\0';
        vscreen->line_changed[vscreen->cur_line] = 1;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "K") || !strcmp(escval, "0K") || !strcmp(escval, "1P") ) {
        //Esc[K = Erase Line: Clears all characters from the cursor position to the end of the line (including the character at the cursor position).
        if (print_error) {fprintf(stderr, "matched to ESC[K or Esc[0K or Esc[1P\n");}
        char *line = line = vscreen->lines[vscreen->cur_line];
        for (int i = vscreen->cur_col; i < vscreen->num_cols; i++) line[i] = '\0';
        vscreen->line_changed[vscreen->cur_line] = 1;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "1K")) {
        //Erace from the line to the begining of the cursor
        if (print_error) {fprintf(stderr, "matched to ESC[1K\n");}
        char *line = vscreen->lines[vscreen->cur_line];
        for (int i = 0; i <= vscreen->cur_col; i++) line[i] = '\0';
        vscreen->line_changed[vscreen->cur_line] = 1;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "2K")) {
        //Erace entire line
        if (print_error) {fprintf(stderr, "matched to ESC[2K\n");}
        char *line = line = vscreen->lines[vscreen->cur_line];
        for (int i = 0; i < vscreen->num_cols; i++) line[i] = '\0';
        vscreen->line_changed[vscreen->cur_line] = 1;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, ";H") || !strcmp(escval, ";f") || !strcmp(escval, "H") || !strcmp(escval, "f") ) {
        if (print_error) {fprintf(stderr, "matched to Esc[;H or Esc[;f or Esc[H or ESC[f\n");}
        vscreen->cur_line = vscreen->cur_col = 0;
        esc_control_reset();
        return;
    } else if ( !strcmp(escval, "0A") ) {
        //Line Feed
        if (print_error) {fprintf(stderr, "matched to Esc[0A.\n");}
        vscreen->cur_line++;
        if (vscreen->cur_line == vscreen->num_lines) {
            vscreen_scroll(vscreen);
            vscreen->cur_line = vscreen->num_lines - 1;
        }
        int col = vscreen->cur_col;
        vscreen->cur_col = 0;
        for (int i = 0; i < col; i++) vscreen_putc(vscreen, ' ');
        esc_control_reset();
        return;
    } else if (!strcmp(escval, "A")) {
        //Esc[A    Cursor Up:
        if (print_error) {fprintf(stderr, "matched to Esc[A.\n");}
        vscreen->cur_line--;
        if ( vscreen->cur_line < 0) vscreen->cur_line = 0;
        esc_control_reset();
        return;
    } else if (!strcmp(escval, "B")) {
        //Esc[B    Cursor Down:
        if (print_error) {fprintf(stderr, "matched to Esc[B.\n");}
        vscreen->cur_line++;
        if ( vscreen->cur_line >= (vscreen->num_lines) ) vscreen->cur_line = (vscreen->num_lines - 1);
        esc_control_reset();
        return;
    } else if (!strcmp(escval, "C")) {
        //Esc[C    Cursor Forward:
        if (print_error) {fprintf(stderr, "matched to Esc[C.\n");}
        vscreen->cur_col++;
        if ( vscreen->cur_col >= vscreen->num_cols) vscreen->cur_col = (vscreen->num_cols - 1);
        esc_control_reset();
        return;
    } else if (!strcmp(escval, "D")) {
        //Esc[D    Cursor Backward:
        if (print_error) {fprintf(stderr, "matched to Esc[D.\n");}
        vscreen->cur_col--;
        if ( vscreen->cur_col < 0) vscreen->cur_col = 0;
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'A') != NULL ) { //else if (sscanf(escval, "%dA", &val1) == 1 ) {
        //Esc[ValueA    Cursor Up:
        if (print_error) {fprintf(stderr, "reached to Esc[ValueA. ");}
        if (sscanf(escval, "%dA", &val1) == 1 ) {
            if (print_error) {fprintf(stderr, "and matched. Value %d\n", val1);}
            val2 = vscreen->cur_line - val1;
            val1 = (val2 >= 0) ? val2 : 0;
            vscreen->cur_line = val1;
        }
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'B') != NULL ) { //else if (sscanf(escval, "%dB", &val1) == 1 ) {
        //Esc[ValueB    Cursor down:
        if (print_error) {fprintf(stderr, "reached to Esc[ValueB. ");}
        if (sscanf(escval, "%dB", &val1) == 1 ) {
            if (print_error) {fprintf(stderr, "and matched. Value %d\n", val1);}
            val2 = vscreen->cur_line + val1;
            val1 = (val2 < (vscreen->num_lines - 1)) ? val2 : (vscreen->num_lines - 1);
            vscreen->cur_line = val1;
        }
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'C') != NULL ) {//else if (sscanf(escval, "%dC", &val1) == 1 ) {
        //Esc[ValueC    Cursor Forward:
        if (print_error) {fprintf(stderr, "reached to Esc[ValueC. ");}
        if (sscanf(escval, "%dC", &val1) == 1 ) {
            if (print_error) {fprintf(stderr, "and matched. Value %d\n", val1);}
            val2 = vscreen->cur_col + val1;
            val1 = (val2 < (vscreen->num_cols)) ? val2 : (vscreen->num_cols - 1);
            vscreen->cur_col = val1;
        }
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'D') != NULL ) {//else if (sscanf(escval, "%dD", &val1) == 1 ) {
        //Esc[ValueD    Cursor Backward:
        if (print_error) {fprintf(stderr, "reached to Esc[ValueD. ");}
        if (sscanf(escval, "%dD", &val1) == 1 ) {
            if (print_error) {fprintf(stderr, "and matched. Value %d\n", val1);}
            val2 = vscreen->cur_col - val1;
            val1 = (val2 >= 0) ? val2 : 0;
            vscreen->cur_col = val1;
        }
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'P') != NULL ) {
        //Esc[ValueP    Delete N char to right
        if (print_error) {fprintf(stderr, "reached to Esc[ValueP. ");}
        if (sscanf(escval, "%dP", &val1) == 1 ) {
            if (print_error) {
                fprintf(stderr, "and matched. Value %d.\n", val1);
                char *line = vscreen->lines[vscreen->cur_line];
                for (int i = vscreen->cur_col; i < vscreen->num_cols; i++) line[i] = '\0';
                vscreen->line_changed[vscreen->cur_line] = 1;
                // for (int i = vscreen->cur_col; val1 > 0 && i < vscreen->num_cols; i++) {
                //     fprintf(stderr, "before string is %s\n", line );
                //     line[i] = '\0';
                //     val1--;
                //     fprintf(stderr, "after string is %s\n", line );
                // }
                // vscreen->line_changed[vscreen->cur_line] = 1;
            }
            esc_control_reset();
            return;
        }
    } else if ( strchr(escval, 'H') != NULL || strchr(escval, 'f') != NULL ) {//else if (sscanf(escval, "%d;%d[H,f]", &val1, &val2) == 2 ) {
        //Esc[Line;ColumnH = Cursor position
        if (print_error) {fprintf(stderr, "reached to Esc[Line;ColumnH or Esc[Line;Columnf ");}
        if (sscanf(escval, "%d;%d[H,f]", &val1, &val2) == 2 ) {
            if (print_error) {fprintf(stderr, "and matched. Value1 %d Value2 %d\n", val1, val2);}
            if (val1 >= 0 && val1 < vscreen->num_lines) vscreen->cur_line = val1;
            if (val2 >= 0 && val2 < vscreen->num_cols) vscreen->cur_col = val2;
        }
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'm') != NULL ) {
        //Esc[Value;...;Valuem  Set Graphics Mode:
        if (print_error) {fprintf(stderr, "matched to Esc[m. Unsupported\n");}
        // vscreen->cur_line++;
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'h') != NULL ) {
        //Esc[Valueh   Set Mode:
        if (print_error) {fprintf(stderr, "matched to Esc[Valueh. Unsupported\n");}
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'l') != NULL ) {
        //Esc[Valuel   Reset Mode:
        if (print_error) {fprintf(stderr, "matched to Esc[Valuel. Unsupported\n");}
        esc_control_reset();
        return;
    } else if ( strchr(escval, 'p') != NULL ) {
        //Esc[Code;String;...p  Set Keyboard Strings:
        if (print_error) {fprintf(stderr, "matched to Esc[Code;String;...p. Unsupported\n");}
        esc_control_reset();
        return;
    } else if ( escval_index == 9) {
        esc_control_reset();
        return;
    }
}

void esc_control_reset() {
    escval_index = 0;
    esc_entered = 0;
    esc_seq = 0;
    val1 = 0;
    val2 = 0;
    for (int i = 0; i < 10; i++) escval[i] = '\0';
}