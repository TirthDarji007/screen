#include <ncurses.h>

#include "session.h"
#include "vscreen.h"

/*
 * The control character used to escape program commands,
 * so that they are not simply transferred as input to the
 * foreground session.
 */
#define COMMAND_ESCAPE 0x1   // CTRL-A
#define MAX_ARGV 20

int mainloop(void);
void do_command(void);
void do_other_processing(void);
static void finalize(void);

//Helpfer function defined by Tirth
void parse_args(int argc, char *argv[]);
void create_terminal();
void switch_screen(int sid);
void setup_signals();
void handle_kill();
void handle_sigterm();
void handle_sigalarm();
void handle_sigchld(int pid);
void input_args();
void display_help();