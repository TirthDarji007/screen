#ifndef VSCREEN_H
#define VSCREEN_H

/*
 * Data structure maintaining information about a virtual screen.
 */

#include <ncurses.h>

typedef struct vscreen VSCREEN;

extern WINDOW *main_screen;
extern WINDOW *status_screen;
extern char *status_line;
extern int print_error;

VSCREEN *vscreen_init(void);
void vscreen_show(VSCREEN *vscreen);
void vscreen_sync(VSCREEN *vscreen);
void vscreen_putc(VSCREEN *vscreen, char c);
void vscreen_fini(VSCREEN *vscreen);

void vscreen_scroll(VSCREEN *vscreen) ;
void vscreen_clear_line(VSCREEN *vscreen, int l);
void set_status(char *status);
void update_time();
void handle_escseq(VSCREEN *vscreen,char ch);
void esc_control_reset();
#endif
